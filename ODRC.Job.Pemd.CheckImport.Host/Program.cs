﻿using Topshelf;
using System;
using ODRC.Job.Pemd.CheckImport.Core;

namespace ODRC.Job.Pemd.CheckImport.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<CheckImportService>(s =>
                {
                    s.ConstructUsing(name => new CheckImportService());
                    s.WhenStarted(ss => ss.Start());
                    s.WhenStopped(ss => ss.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Imports the Check details for a claim like Payment Number and Payment Date from the YA Check Import file.");
                x.SetDisplayName("ODRC.Job.Pemd.CheckImport.Service");
                x.SetServiceName("ODRC.Job.Pemd.CheckImport.Service");
            });
        }
    }
}
