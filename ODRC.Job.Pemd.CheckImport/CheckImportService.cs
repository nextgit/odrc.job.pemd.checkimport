﻿using Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using ODRC.Job.Pemd.CheckImport.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Timers;

namespace ODRC.Job.Pemd.CheckImport.Core
{
    public class CheckImportService
    {
        private readonly Timer _timer;
        private StringLogger _logger;

        public CheckImportService()
        {
            _timer = new Timer(30000) { AutoReset = true };

            _timer.Elapsed += (sender, eventArgs) =>
            {
                _logger = new StringLogger(LogManager.GetLogger("ODRC.Job.Pemd.CheckImpoort"));
                
                // Key in method that would be run in the set interval
            };
        }

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
    }
}
