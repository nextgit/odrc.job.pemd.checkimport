### What is this repository for? ###

This is windows service that handles importing Check files for YA into the  ODRC system. These check files update the Claim's payment details like Check Number and Payment date.

### How do I get set up? ###

1. Get the latest version of the project
2. Build & Compile
3. Hit F5

### Contribution guidelines ###

* Do whatever you like... Just dont mess around not knowing what you're doing!
* When in doubt, always ask for a code review!

### Who do I talk to? ###

* Talk to Stephen